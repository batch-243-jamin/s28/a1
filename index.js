/*- name - single
- accomodates - 2
- price - 1000
- description - A simple room with all the basic necessities
- rooms_available - 10
- isAvailable - false
*/

db.Rooms.insert({
	name: "Single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities.",
	rooms_available: 10,
	isAvailable: false
});

/* Insert multiple rooms (insertMany method) with the following details:
- name - double
- accomodates - 3
- price - 2000
- description - A room fit for a small family going on a vacation
- rooms_available - 5
- isAvailable - false

- name - queen
- accomodates - 4
- price - 4000
- description - A room with a queen sized bed perfect for a simple getaway
- rooms_available - 15
- isAvailable - false
*/

db.Rooms.insertMany([{
	name: "Double",
	accomodates: 3,
	price: 2000,
	description: "A room fit for a small family on a vacation.",
	rooms_available: 5,
	isAvailable: false
}, {
	name: "Queen",
	accomodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple gateaway.",
	rooms_available: 15,
	isAvailable: false

	}]);

// Use the find method to search for a room with the name double.

db.Rooms.find({name: "Double"});

// Use the updateOne method to update the queen room and set the available rooms to 0.

db.Rooms.updateOne({
	name: "Queen"},
	{
		$set:{
			rooms_available: 0
		}
	}
);